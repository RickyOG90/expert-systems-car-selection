;----------------------------------------------------------------------------------------------------
;	Car Type Identification
;
;	Riccardo Gutierrez U70795314
;	Saurabh Hinduja U34041272
;
;	Description: This CLIPS expert system is designed to help a user determine the class of car they
;				 should buy based off of their needs as well as their living situation
;----------------------------------------------------------------------------------------------------

;--------------------------------------------------
;                      Classes
;--------------------------------------------------

(defclass person (is-a USER) ;first part of information gathered from the user
	(role concrete)
	(slot Name)
	(slot Gender)
	(slot Married) 
	(slot income)
	(slot kids)
	(slot num-kids (default "0") (type STRING))
	(slot power)
)

(defclass usage (is-a USER) ;second part of information gathered from the user
	(role concrete)
	(slot roadtrip)
	(slot mpg)
	(slot fishing)
	(slot comfort)
	(slot carpool (default "no"))
	(slot kids-sports (default "no"))
	(slot commute-distance)
	(slot work)
	(slot type-work (default "none"))
)

(deftemplate typ-CAR ;the template for the categories of the various cars to choose from
	(slot name (default none))
	(slot class (default none))
	(slot passengers (default low))
	(slot cargo_capacity (default low) )
	(slot mpg (default high))
	(slot sports)
	(slot range)
	(slot towing_capacity)
	(slot cost)
	(slot comfort)
)

;--------------------------------------------------
;                  Car Facts
;--------------------------------------------------

(deffacts CARS ;this is where we have our current list of cars that the program will choose from. This can be expanded to add more cars to create a more thorough and accurate program
	(typ-CAR(name "Tesla Model S P100D") (class "a Luxury Full-Electric") (cost high) (passengers medium) (cargo_capacity  medium) (mpg high) (sports yes) (towing_capacity low) (comfort high)(range low) )
	(typ-CAR(name "Gensis G90") (class "a Luxury 4 door") (cost high) (passengers medium) (cargo_capacity  medium) (mpg low) (sports yes) (towing_capacity low) (comfort high)(range medium) )
	(typ-CAR(name "Porche 911 Carrera S") (class "a Sporty 2 Door") (cost high) (passengers low) (cargo_capacity  low) (mpg low) (sports yes) (towing_capacity low) (comfort medium) (range medium) )
	(typ-CAR(name "Ford F150 4 Door") (class "a 4 door pickup") (cost medium) (passengers medium) (cargo_capacity  high) (mpg medium) (sports no) (towing_capacity high) (comfort medium)(range medium) )
	(typ-CAR(name "Toyota Highlander") (class "a SUV") (cost medium) (passengers medium) (cargo_capacity  medium) (mpg medium) (sports no) (towing_capacity medium) (comfort medium) (range medium))
	(typ-CAR(name "Nissan LEAF") (class "an Affordable Full-Electric") (cost medium) (passengers medium) (cargo_capacity  low) (mpg high) (sports no) (towing_capacity low) (comfort low) (range low))
	(typ-CAR(name "Ford F150" ) (class "a 2 Door pickup") (cost medium) (passengers low) (cargo_capacity  high) (mpg medium) (sports no) (towing_capacity high) (comfort medium) (range high))
	(typ-CAR(name "Honda Civic Type R") (class "a Sporty Hatchback") (cost medium) (passengers low) (cargo_capacity  medium) (mpg medium) (sports yes) (towing_capacity low) (comfort low) (range high))
	(typ-CAR(name "Chrysler Pacifica") (class "a Minivan") (cost low) (passengers high) (cargo_capacity  medium) (mpg medium) (sports no) (towing_capacity medium) (comfort medium) (range medium))
	(typ-CAR(name "Toyota Prius") (class "a Hybrid") (cost low) (passengers medium) (cargo_capacity  medium) (mpg high) (sports no) (towing_capacity low) (comfort low) (range high))
	(typ-CAR(name "Nissan Versa Sedan") (class "an affordable 4 door") (cost low) (passengers medium) (cargo_capacity  medium) (mpg medium) (sports no) (towing_capacity low) (comfort low) (range high))
	(typ-CAR(name "Ford Fiesta") (class "an Affordable 5 door Hatchback") (cost low) (passengers medium) (cargo_capacity  low) (mpg medium) (sports no) (towing_capacity low) (comfort medium) (range high))
	(typ-CAR(name "Fiat 500") (class "an Affordable Compact")(cost low) (passengers low) (cargo_capacity  low) (mpg high) (sports no) (towing_capacity low) (comfort low) (range high))
)

;--------------------------------------------------
;                  Instances
;--------------------------------------------------

(definstances PERSON_OBJECT ;our instance for the user
	(human of person)
)

(definstances USAGE-OBJECT ;our instance for the user's other options
	(features of usage)
)

;--------------------------------------------------
;                Ask questions functions
;--------------------------------------------------

(deffunction ask-question (?question $?allowed-values) ;default ask-question function taken from slides
	(printout t ?question)
	(bind ?answer (read))
	(if (lexemep ?answer) 
		then (bind ?answer (lowcase ?answer)))
	(while (not (member ?answer ?allowed-values)) do
		(printout t ?question)
		(bind ?answer (read))
		(if (lexemep ?answer) 
			then (bind ?answer (lowcase ?answer))))
	?answer
)

(deffunction normal-question (?question) ;this function is used to take in the user's name so as to add a bit of finesse to the software
	(printout t ?question)
	(bind ?answer (read))
	?answer
)

(deffunction yes-or-no-p (?question) ;default ask-question function taken from slides
	(bind ?response (ask-question ?question yes no y n))
	(if (or (eq ?response yes) (eq ?response y))
		then yes 
		else no)
)

;--------------------------------------------------
;           Functions to Print result  
;--------------------------------------------------

(deffunction printresult() ;this function will print the results generated from the program after the questions have been asked
	(printout t "Car Suggestions are: " crlf)
	(do-for-all-facts ((?f typ-CAR)) TRUE (printout t ?f:class " like a " ?f:name crlf))
	(halt)
)

(deffunction number() ;this function counts how many facts(cars) remain as they are eliminated from possible choices
	(bind ?k (length$ (find-all-facts ((?f typ-CAR)) TRUE)))
	(if (> ?k 1) 
		then
			(return ?k)
		else
			(printresult)
	)
)

;--------------------------------------------------
;               Question Rules
;--------------------------------------------------

(defrule GetName ;this rule is what retrieves the user's name
	(declare (salience 500))
	=>
	(send [human] put-Name ( normal-question "What is your name? "))
)

(defrule message ;this rule responds back using the user's name
	(declare (salience 450))
	=> (printout t "Welcome " (send [human] get-Name) "  Lets get you a new car!" crlf "We will suggest you a new car type" crlf)
)

(defrule gender ;this rule queries the user for their gender 
	(declare (salience 400))
	=>
	(send [human] put-Gender (ask-question "What is your gender?(male/female) " male female))
)

(defrule marriage ;this rule queries the user's marital status
	(declare (salience 350))
	=>
	(send [human] put-Married (yes-or-no-p "Are you married?(yes/no) " ))
)

(defrule income ;this rule queries the user's income
	(declare (salience 300))
	=>
	(send [human] put-income (yes-or-no-p "Do you have a lot of disposable income?(yes/no) " ))
)

(defrule kids ;this rule queries if the user has kids
	(declare (salience 275))
	=>
	(send [human] put-kids (yes-or-no-p "Do you have kids?(yes/no) " ))
)

(defrule numKids ;this rule queries the user how many kids they have, ranging from 1 to 8
	(declare (salience 260))
	=>
	(if (= (str-compare (send [human] get-kids) "yes") 0) then
	(send [human] put-num-kids (ask-question "How many kids do you have?(1-8) " 1 2 3 4 5 6 7 8)))
)

(defrule kids-sports ;this rule queries the user if their kids have any extracurricular activities as this may require higher cargo capacity
	(declare (salience 250))
	=>
	(if (= (str-compare (send [human] get-kids) "yes") 0) then
	(send [features] put-kids-sports (yes-or-no-p "Do your children participate in sports or extracurricular activities?(yes/no) ")))
)

(defrule fuelEco ;this rule queries the user if they need fuel economy in the car they want. Granted this is more subjective as other questions can end up overruling the ability to have high MPG
	(declare (salience 230))
	=>
	(send [features] put-mpg (yes-or-no-p "Do you need high fuel economy?(yes/no) "))
)

(defrule roadtrips ;this rule queries the user if they go on roadtrips as this may affect cargo capacity and comfort needs
	(declare (salience 220))
	=>
	(send [features] put-roadtrip (yes-or-no-p "Do you go on long roadtrips?(yes/no) "))
)

(defrule PowerAv ;this rule queries the user if they need a lot of power. Some people enjoy sportier cars and want that rather than a sluggish vehicle.
	(declare (salience 210))
	=>
	(send [human] put-power (yes-or-no-p "Do you need plenty of power(as in horsepower or torque)?(yes/no) "))
)

(defrule fishingpref ;this rule queries the user if they go fishing because the logic would mean that 1. they need more cargo capacity, and 2. they might be carrying a boat which means more towing capacity in a vehicle
	(declare (salience 200))
	=>
	(send [features] put-fishing (yes-or-no-p "Do you enjoy fishing?(yes/no) "))
)

(defrule comfort ;this rule queries the user their preference on comfort but once again this can be overruled from other more important responses
	(declare (salience 190))
	=>
	(send [features] put-comfort (yes-or-no-p "Do you need a smooth/comfortable ride?(yes/no) "))
)

(defrule job ;this rule queries the user about their job status as this can influence the price of the car they can afford
	(declare (salience 180))
	=>
	(send [features] put-work (yes-or-no-p "Do you work? "))
)

(defrule worktype ;this rule queries the user about their job, essentially if they use their vehicle for the job or just to get to their job
	(declare (salience 170))
	=>
	(if (= (str-compare (send [features] get-work) "yes") 0) then
	(send [features] put-type-work (ask-question "Does your work require you to haul equipment/supplies or carry passengers?(supplies/passengers/none) " supplies passengers none)))
)

(defrule carpooling ;this rule queries the user if they carpool as this can increase passenger capacity
	(declare (salience 160))
	=>
	(if (= (str-compare (send [human] get-kids) "no") 0) then
	(send [features] put-carpool (yes-or-no-p "Do you carpool often?(yes/no) ")))
)

(defrule commute ;this rule queries the user about their daily commute as this can end up eliminating electric vehicles
	(declare (salience 150))
	=>
	(send [features] put-commute-distance (ask-question "Is your daily commute long or short distance?(long/short) " long short none))
)

;--------------------------------------------------
;                  Vehicle Rules
;--------------------------------------------------

(defrule passengerLow ;this rule essentially eliminates any vehicle with high passenger capacity based on certain conditions 
	(declare (salience 159))
	?e <- (typ-CAR(passengers high))
	=>
	(if (and	(=(str-compare(send [human] get-kids) "no") 0 )
				(=(str-compare(send [features] get-carpool) "no") 0 )
		)
	then
		(retract ?e)
	else
		(if (and	(=(str-compare(send [features] get-carpool) "no") 0 )
					(<(str-compare(str-cat(send [human] get-num-kids)) "3") 0 )	
			)
		then
			(retract ?e)
		)
	)
	(number)
)

(defrule passengerLow2 ;this rule essentially eliminates any vehicle with medium passenger capacity based on certain conditions
	(declare (salience 159))
	?e <- (typ-CAR(passengers medium))
	=>
	(if (and	(=(str-compare(send [human] get-kids) "no") 0 )
				(=(str-compare(send [features] get-carpool) "no") 0 )
		)
	then
		(retract ?e)
	else
		(if (and	(=(str-compare(send [features] get-carpool) "no") 0 )
					(<(str-compare(str-cat(send [human] get-num-kids)) "3") 0 )	
			)
		then
			(retract ?e)
		)
	)
	(number)
)

(defrule passengerMed ;this rule essentially eliminates any vehicle with low passenger capacity based on given conditions
	(declare (salience 159))
	?e <- (typ-CAR(passengers low))
	=>
	(if (and	(=(str-compare(send [human] get-kids) "yes") 0 )
				(<(str-compare(str-cat(send [human] get-num-kids)) "4") 0 )
		)
	then
			(retract ?e)
	else
		(if(or	(=(str-compare(send [features] get-type-work) "passengers") 0 )
				(=(str-compare(send [features] get-carpool) "yes") 0 )
		)
		then
				(retract ?e)
		)
	)
)

(defrule passengerMed2 ;this rule essentially eliminates any vehicle with high passenger capacity based on certain conditions
	(declare (salience 159))
	?e <- (typ-CAR(passengers high))
	=>
	(if (and	(=(str-compare(send [human] get-kids) "yes") 0 )
				(<(str-compare(str-cat(send [human] get-num-kids)) "4") 0 )
		)
	then
			(retract ?e)
	else
		(if(or	(=(str-compare(send [features] get-type-work) "passengers") 0 )
				(=(str-compare(send [features] get-carpool) "yes") 0 )
		)
		then
			(retract ?e)
		)
	)
)

(defrule passengerHigh ;this rule essentially eliminates any vehicle with low passenger capacity based on certain conditions
	(declare (salience 159))
	?e <- (typ-CAR(passengers low))
	=>
	(if (and	(=(str-compare(send [human] get-Married) "yes") 0 )
				(=(str-compare(send [human] get-kids) "yes") 0 )
		)
	then
		(if (>(str-compare(str-cat(send [human] get-num-kids)) "3") 0 )
		then
			(retract ?e)
		else
			(if(or		(=(str-compare(send [features] get-type-work) "passengers") 0 )
						(=(str-compare(send [features] get-carpool) "yes") 0 )
				)
			then
				(retract ?e)
			)
		)
	)
	(number)
)

(defrule passengerHigh2 ;this rule essentially eliminates any vehicle with medium passenger capacity based on certain conditions
	(declare (salience 159))
	?e <- (typ-CAR(passengers medium))
	=>
	(if (and	(=(str-compare(send [human] get-Married) "yes") 0 )
				(=(str-compare(send [human] get-kids) "yes") 0 )
		)
	then
		(if (>(str-compare(str-cat(send [human] get-num-kids)) "3") 0 )
		then
			(retract ?e)
		else
			(if(or		(=(str-compare(send [features] get-type-work) "passengers") 0 )
						(=(str-compare(send [features] get-carpool) "yes") 0 )
				)
			then
				(retract ?e)
			)
		)
	)
	(number)
)


(defrule cargo-capacityMed  ;this rule essentially eliminates any vehicle with low cargo capacity based on certain conditions
	(declare (salience 162))
	?e <- (typ-CAR(cargo_capacity low))
	=>
	(if (and	(=(str-compare(send [human] get-Married) "yes") 0 )
				(=(str-compare(send [human] get-kids) "yes") 0 )
		)
	then
		(if (<(str-compare(str-cat(send [human] get-num-kids)) "4") 0 )
		then
			(retract ?e)
		)
	else
		(if (or	(=(str-compare(send [features] get-carpool) "yes") 0 )
				(=(str-compare(send [features] get-type-work) "supplies") 0 )
			)
		then
			(retract ?e)
		)
	)
	(number)
)

(defrule cargo-capacityHigh ;this rule essentially eliminates any vehicle with low cargo capacity based on certain conditions
	(declare (salience 162))
	?e <- (typ-CAR(cargo_capacity low))
	=>
	(if (and	(=(str-compare(send [human] get-Married) "yes") 0 )
				(=(str-compare(send [human] get-kids) "yes") 0 )
		)
	then
		(if (and(>(str-compare(str-cat(send [human] get-num-kids)) "3") 0 )
				(=(str-compare(send [features] get-kids-sports) "yes") 0 )
			)
		then
			(retract ?e)
		)
	)
	(number)
)

(defrule cargo-capacityHigh2 ;this rule essentially eliminates any vehicle with medium cargo capacity based on certain conditions
	(declare (salience 162))
	?e <- (typ-CAR(cargo_capacity medium))
	=>
	(if (and	(=(str-compare(send [human] get-Married) "yes") 0 )
				(=(str-compare(send [human] get-kids) "yes") 0 )
		)
	then
		(if (and(>(str-compare(str-cat(send [human] get-num-kids)) "3") 0 )
				(=(str-compare(send [features] get-kids-sports) "yes") 0 )
			)
		then
			(retract ?e)
		)
	)
	(number)
)

(defrule mpgMed  ;this rule essentially eliminates any vehicle with low MPG vehicles based on certain conditions
	(declare (salience 35))
	?e <- (typ-CAR(mpg low))
	=>
	(if (and	(=(str-compare(send [features] get-mpg) "no") 0 )
				(=(str-compare(send [features] get-commute-distance) "long") 0 )
		)
	then 
		(retract ?e)
	)
	(number)
)

(defrule mpgHigh  ;this rule essentially eliminates any vehicle with low MPG vehicles based on certain conditions
	(declare (salience 35))
	?e <- (typ-CAR(mpg low))
	=>
	(if (or	(=(str-compare(send [features] get-mpg) "yes") 0 )
			(=(str-compare(send [features] get-commute-distance) "long") 0 )
		)
	then 
		(retract ?e)
	)
	(number)
)

(defrule range ;this rule essentially eliminates any vehicle with a low range (i.e. electric vehicles) based on certain conditions
	(declare(salience 215))
	?e <- (typ-CAR(range low))
	=>
	(if (=(str-compare(send [features] get-roadtrip) "yes") 0 )
	then 
		(retract ?e)
	)
	(number)
)

(defrule towing-capacityLow ;this rule essentially eliminates any vehicle with high towing capacity based on certain conditions
	(declare (salience 161))
	?e <- (typ-CAR(towing_capacity high))
	=>
	(if (or		(=(str-compare(send [features] get-fishing) "no") 0 )
				(=(str-compare(send [features] get-type-work) "none") 0 )
		)
	then
		(retract ?e)
	)
	(number)
)

(defrule towing-capacityMed ;this rule essentially eliminates any vehicle with low towing capacity based on certain conditions
	(declare (salience 161))
	?e <- (typ-CAR(towing_capacity low))
	=>
	(if (or		(=(str-compare(send [features] get-fishing) "yes") 0 )
				(=(str-compare(send [features] get-type-work) "supplies") 0 )
		)
	then
		(retract ?e)
	)
	(number)
)

(defrule towing-capacityHigh ;this rule essentially eliminates any vehicle with medium towing capacity based on certain conditions
	(declare (salience 161))
	?e <- (typ-CAR(towing_capacity medium))
	=>
	(if (and	(=(str-compare(send [features] get-fishing) "yes") 0 )
				(=(str-compare(send [features] get-type-work) "supplies") 0 )
		)
	then
		(retract ?e)
	)
	(number)
)

(defrule towing-capacityHigh2 ;this rule essentially eliminates any vehicle with low towing capacity based on certain conditions
	(declare (salience 161))
	?e <- (typ-CAR(towing_capacity low))
	=>
	(if (and	(=(str-compare(send [features] get-fishing) "yes") 0 )
				(=(str-compare(send [features] get-type-work) "supplies") 0 )
		)
	then 
		(retract ?e)
	)
	(number)
)

(defrule costLow ;this rule essentially eliminates any vehicle with a high cost based on certain conditions
	(declare (salience 175))
	?f <-(typ-CAR(cost high))
	=> 
	(if (and	(=(str-compare(send [human] get-income) "no") 0 )
				(=(str-compare(send [features] get-work) "no") 0 )
		)
	then
		(retract ?f)
		(number)		
	)
)

(defrule costLow2 ;this rule essentially eliminates any vehicle with a medium cost based on certain conditions
	(declare (salience 174))
	?e <- (typ-CAR(cost medium))
	=>
	(if (and	(=(str-compare(send [human] get-income) "no") 0 )
				(=(str-compare(send [features] get-work) "no") 0 )
		)
	then
		(retract ?e)
		(number)		
	)
)

(defrule costMed  ;this rule essentially eliminates any vehicle with high cost based on certain conditions
	(declare (salience 80))
	?f <-(typ-CAR(cost high))
	=>
	(if (and	(=(str-compare(send [human] get-income) "yes") 0 )
				(=(str-compare(send [features] get-work) "yes") 0 )
		)
	then
		(if (>(str-compare(str-cat(send [human] get-num-kids)) "3") 0 )
		then
			(retract ?f)
		)
	else
		(if	(and	(=(str-compare(send [human] get-income) "no") 0 )
					(=(str-compare(send [features] get-work) "yes") 0 )
			)
		then
			(if (=(str-compare(send [human] get-kids) "no") 0 )
			then
				(retract ?f)
			)
		)
	)
)

(defrule comfortMed  ;this rule essentially eliminates any vehicle with low comfort based on certain conditions
	(declare (salience 35))
	?f <-(typ-CAR(comfort low))
	=>
	(if (or		(=(str-compare(send [human] get-income) "yes") 0 )
				(=(str-compare(send [features] get-roadtrip) "yes") 0 )
		)
	then
		(if(or	(=(str-compare(send [features] get-comfort) "yes") 0 )
				(=(str-compare(send [features] get-commute-distance) "short") 0 )
			)
		then
		(retract ?f)
		)
	)
	(number)
)

(defrule comfortHigh ;this rule essentially eliminates any vehicle with low comfort based on certain conditions
	(declare (salience 15))
	?f <-(typ-CAR(comfort low))
	=>
	(if (and	(=(str-compare(send [human] get-income) "yes") 0 )
				(=(str-compare(send [features] get-comfort) "yes") 0 )
		)
	then
		(if (or	(=(str-compare(send [features] get-roadtrip) "yes") 0 )
				(=(str-compare(send [features] get-commute-distance) "long") 0 )
			)
		then
			(retract ?f)
		)
	)
	(number)
)

(defrule sportiness ;this rule essentially eliminates any vehicle with or without sportiness based on certain conditions
	(declare (salience 145))
	?f <-(typ-CAR(sports no))
	?e <-(typ-CAR(sports yes))
	=>
	(if (and	(=(str-compare(send [human] get-income) "yes") 0 )
				(=(str-compare(send [features] get-mpg) "no") 0 )
		)
	then
		(if	(and	(=(str-compare(send [features] get-fishing) "no") 0 )
					(=(str-compare(send [features] get-type-work) "none") 0 )
			)
		then
			(retract ?f)
		)
	else
		(retract ?e) 
	)
	(number)
)

(defrule results ;this rule essentially calls the function to print the final results
	(declare (salience -100))
	=>
	(printresult)
)